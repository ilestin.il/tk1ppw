from django.urls import path
from .views import app_base


urlpatterns = [
    path('', app_base, name='app_base'),
]
