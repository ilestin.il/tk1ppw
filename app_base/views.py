from django.shortcuts import render

# Create your views here.
response = {}


def app_base(request):
    return render(request, 'app_base.html', response)
